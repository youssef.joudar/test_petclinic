package org.springframework.samples.petclinic.owner;

import java.util.ArrayList;
import java.util.List;

import org.springframework.samples.petclinic.visit.Visit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PetImportJson {


	private Owner owner;
	private List <Pet> petList = new ArrayList<Pet>();
	
	
	public PetImportJson(Owner owner, List<Pet> petList) {
		super();
		this.owner = owner;
		this.petList = petList;
	}
	
	public PetImportJson() {
		super();
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	public List<Pet> getPetList() {
		return petList;
	}
	public void setPetList(List<Pet> petList) {
		this.petList = petList;
	}
}
