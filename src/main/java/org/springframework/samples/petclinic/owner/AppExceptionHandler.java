//package org.springframework.samples.petclinic.owner;
//
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//@ControllerAdvice
//public class AppExceptionHandler {
//
//    @ExceptionHandler(FileImportException.class)
//    public ModelAndView handleException(FileImportException exception, RedirectAttributes redirectAttributes) {
//
//        ModelAndView mav = new ModelAndView();
//        mav.addObject("message", exception.getMsg());
//        mav.setViewName("welcome");
//        return mav;
//    }
//}
