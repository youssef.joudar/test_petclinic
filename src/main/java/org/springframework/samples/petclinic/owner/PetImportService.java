package org.springframework.samples.petclinic.owner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.samples.petclinic.treatment.Treatment;
import org.springframework.samples.petclinic.treatment.TreatmentRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.text.DecimalFormat;
import java.time.LocalDate;

@Service
public class PetImportService {
	

	private  PetRepository petRepository;

	private  OwnerRepository ownerRepository;
	
	private  VisitRepository visitRepository;
	

	private TreatmentRepository treatmentRepository ;

	
	public PetImportService(PetRepository petRepository, OwnerRepository ownerRepository,
			VisitRepository visitRepository, TreatmentRepository treatmentRepository) {
		this.petRepository = petRepository;
		this.ownerRepository = ownerRepository;
		this.visitRepository = visitRepository;
		this.treatmentRepository = treatmentRepository;
	}

	public List<PetImportJson> constructJsonFiles(MultipartFile[] files) {
		List<PetImportJson> importJsonList = new ArrayList<>();
		try {
			for (MultipartFile file : files) {
				importJsonList.add(new ObjectMapper().readValue(file.getBytes(), PetImportJson.class));
			}
		} catch (Exception e) {
			e.printStackTrace();
			 throw new FileImportException("Could not import files. Please try again!");
		}

		return importJsonList;
	}
	
	public void importPetData(PetImportJson petImportJson) {
		
		if (petImportJson.getPetList()!= null && petImportJson.getPetList().size()>0) {
			int i = 0;
			for (Pet pet : petImportJson.getPetList()) {
				Owner owner = petImportJson.getOwner();
				ownerRepository.save(owner);
				
				owner.addPet(pet);
			
							
				Pet createdPet = petRepository.saveAndFlush(pet);

				saveVisites(createdPet.getId(), pet.getVisits());
				saveTreatments(createdPet.getId(), pet.getTreatments());
				
				System.out.println(i++);
			}
		}
	
	}
	

	private void saveVisites(Integer petId, List<Visit> visits) {
		if (visits!= null && visits.size()>0) {
			for (Visit visit : visits) {
				visit.setVetId(Integer.valueOf(visit.getMatriculeVet().substring(12)));
				visit.setPetId(petId);
				visitRepository.save(visit);
			}
		}
		
	}


	private void saveTreatments(Integer petId, List<Treatment> treatments) {
		if (treatments!= null && treatments.size()>0) {
			for (Treatment  treatment : treatments) {
				 treatment.setVetId(Integer.valueOf(treatment.getMatriculeVet().substring(12)));
				 treatment.setPetId(petId);
				treatmentRepository.save(treatment);
			}
		}
		
	}
	
	public String generatePetImportJson (int ownerNumber, int petNumber) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		
		PetImportJson petImportJson = initiatePetImportJson(ownerNumber, petNumber);
		String jsonString = "Nothing!";
		try {
		jsonString = mapper.writeValueAsString(petImportJson);
		
	} catch (JsonProcessingException e) {
		e.printStackTrace();
		}
		return jsonString;
	}


	public PetImportJson initiatePetImportJson(int ownerNumber, int petNumber) {
		
				
	   String[] petTypes = {"cat", "dog", "lizard", "snake", "bird", "hamster", "Horse"};
		
		Owner owner = null;
		
		PetImportJson importJson = null;
		List<Pet> petList = new ArrayList<Pet>();
		
		
		owner = new Owner();
		Random random = new Random();
		
		
		
		owner.setFirstName("firstName_"+ownerNumber);
		owner.setLastName("lastName_"+ownerNumber);
		owner.setAddress("address_"+ownerNumber);
		owner.setCity("City_"+ownerNumber);
		owner.setTelephone(generatePhoneNumber());
		
		
		for (int j2 = 0; j2 < petNumber; j2++) {
			int randomInt = random.nextInt(6);
			PetType petType = new PetType();
			petType.setId(randomInt+1);
			petType.setName(petTypes[randomInt]);
			
			Pet petData = getNewPet(j2, petType);
			
			petList.add(petData);
		}
		
		
		importJson = new PetImportJson(owner, petList);
		
		return importJson;
	}


	private String generatePhoneNumber() {
		Random rand = new Random();
        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(10000);

        DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
        DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

        return df3.format(num1) + df3.format(num2) + df4.format(num3);

	}


	private Pet getNewPet(int petNumber, PetType petType) {
				
		
		Pet pet = new Pet();
		pet.setName("petName_"+petNumber);
		pet.setBirthDate(LocalDate.now().minusDays(petNumber));
		pet.setType(petType);
		pet.setVisitsInternal(getVisitsForPet(petNumber));
		pet.setTreatmentsInternal(getTreatmentForPet(petNumber));
		return pet;
	}
	
	private List<Visit> getVisitsForPet(int j2) {
		List <Visit> visits = new ArrayList<Visit>();
		Random random = new Random();
		
		for (int i = 0; i < j2+2; i++) {
			int randomInt = random.nextInt(6);
			Visit visit = new Visit();
			
			visit.setDate(LocalDate.now().minusDays(i));
			visit.setDescription("Visit description_"+i);
			visit.setMatriculeVet("matriculeVet"+(randomInt+1));
			visits.add(visit);
		}
		
		return visits;
	}
	
	private static List<Treatment> getTreatmentForPet(int j2) {
		String[] treatmentTypes = {"Tick-borne Diseases", "Heartworms", "Parvo", "animal-assisted therapy", "Cognitive", "Physical", "Psychological"};
		Random random = new Random();
		
		List <Treatment> treatments = new ArrayList<Treatment>();
		
		for (int i = 0; i < j2+2; i++) {
			int randomInt = random.nextInt(6);
			
			Treatment treatment = new Treatment();
			treatment.setType(treatmentTypes[randomInt]);
			treatment.setDate(LocalDate.now().minusDays(i));
			treatment.setMatriculeVet("matriculeVet"+(randomInt+1));
			treatment.setDescription("Treatment description for "+ treatmentTypes[randomInt]+ ".");
			
			treatments.add(treatment);
		}
		
		return treatments;
	}
	
}
