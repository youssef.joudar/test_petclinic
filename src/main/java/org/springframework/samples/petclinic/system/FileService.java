package org.springframework.samples.petclinic.system;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.samples.petclinic.owner.PetImportJson;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class FileService {

	@Value("${app.upload.dir:${user.home}}")
	public String uploadDir;

	public PetImportJson uploadFile(MultipartFile file) {

		try {
//            Path copyLocation = Paths
//                .get(uploadDir + File.separator + StringUtils.cleanPath(file.getOriginalFilename()));
//            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<PetImportJson> constructJsonFiles(MultipartFile[] files) {
		List<PetImportJson> importJsonList = new ArrayList<>();
		try {
			for (MultipartFile file : files) {
				importJsonList.add(new ObjectMapper().readValue(file.getBytes(), PetImportJson.class));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return importJsonList;
	}
}