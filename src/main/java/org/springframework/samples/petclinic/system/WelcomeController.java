/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.system;

import java.util.List;

import org.springframework.samples.petclinic.owner.PetImportJson;
import org.springframework.samples.petclinic.owner.PetImportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class WelcomeController {
	
	//private FileService fileService;
	private PetImportService petImportService;
	
	
	public WelcomeController(PetImportService petImportService) {
		this.petImportService = petImportService;
	}

	@GetMapping("/")
	public String welcome() {
		return "welcome";
	}
	
	@PostMapping("/uploadFiles")
	public String uploadFiles(@RequestParam("files") MultipartFile[] files, RedirectAttributes redirectAttributes) {

		List<PetImportJson> jsonFiles = petImportService.constructJsonFiles(files);
		
		for (PetImportJson petImportJson : jsonFiles) {
			petImportService.importPetData(petImportJson);
		}
		
	    redirectAttributes.addFlashAttribute("message",
	        "You successfully uploaded all files!");

	    return "redirect:/";
	}

}
